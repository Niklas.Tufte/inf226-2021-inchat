package inf226.inchat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;

/**
 * This class stores accounts in the database.
 */
public final class AccountStorage
    implements Storage<Account,SQLException> {
    
    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;
   
    /**
     * Create a new account storage.
     *
     * @param  connection   The connection to the SQL database.
     * @param  userStore    The storage for User data.
     * @param  channelStore The storage for channels.
     */
    public AccountStorage(Connection connection,
                          Storage<User,SQLException> userStore,
                          Storage<Channel,SQLException> channelStore) 
      throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;
        
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT, FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, alias TEXT, ordinal INTEGER, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Account> save(Account account)
      throws SQLException {
        
        final Stored<Account> stored = new Stored<Account>(account);
        String sql = "INSERT INTO Account VALUES(?,?,?,?)";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, stored.identity.toString());
        pre.setString(2, stored.version.toString());
        pre.setString(3, account.user.identity.toString());
        pre.setString(4, account.password.toString());
        pre.executeUpdate( );

        
        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            String msql = "INSERT INTO AccountChannel VALUES(?,?,?,?)";

            try {   
                    PreparedStatement premsql = connection.prepareStatement( msql );
                    premsql.setString(1, stored.identity.toString());
                    premsql.setString(2, channel.identity.toString());
                    premsql.setString(3, alias.toString());
                    premsql.setString(4, ordinal.get().toString());
                    premsql.executeUpdate( );            
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }
    
    @Override
    public synchronized Stored<Account> update(Stored<Account> account,
                                            Account new_account)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Account> current = get(account.identity);
    final Stored<Account> updated = current.newVersion(new_account);
    if(current.version.equals(account.version)) {
        String sql = "UPDATE Account SET" +" (version,user) =(?,?) WHERE id=?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, updated.version.toString());
        pre.setString(2, new_account.user.identity.toString());
        pre.setString(3, updated.identity.toString());

        pre.executeUpdate( );
        
        
        // Rewrite the list of channels
        String delsql = "DELETE FROM AccountChannel WHERE account=?";
        PreparedStatement predel = connection.prepareStatement( delsql );
        predel.setString(1, account.identity.toString());
        predel.executeUpdate( );
        
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        new_account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            String msql = "INSERT INTO AccountChannel VALUES(?,?,?,?)";

            try { 
                    PreparedStatement premsql = connection.prepareStatement( msql );
                    premsql.setString(1, account.identity.toString());
                    premsql.setString(2, channel.identity.toString());
                    premsql.setString(3, alias.toString());
                    premsql.setString(4, ordinal.get().toString());
                    premsql.executeUpdate( );
             }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Account> account)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Account> current = get(account.identity);
        if(current.version.equals(account.version)) {
        String sql =  "DELETE FROM Account WHERE id =?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, account.identity.toString());
        pre.executeUpdate( );
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id)
      throws DeletedException,
             SQLException {

        String accountsql = "SELECT version,user,password FROM Account WHERE id = ?";
        String channelsql = "SELECT channel,alias,ordinal FROM AccountChannel WHERE account = ? ORDER BY ordinal DESC";

        PreparedStatement preaccountStatement = connection.prepareStatement( accountsql );
        PreparedStatement prechannelStatement = connection.prepareStatement( channelsql );

        preaccountStatement.setString(1, id.toString());
        prechannelStatement.setString(1, id.toString());


        final ResultSet accountResult = preaccountStatement.executeQuery( );
        final ResultSet channelResult = prechannelStatement.executeQuery( );

        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid =
            UUID.fromString(accountResult.getString("user"));
            final String password =
            accountResult.getString("password");
            final Stored<User> user = userStore.get(userid);
            // Get all the channels associated with this account
            final List.Builder<Pair<String,Stored<Channel>>> channels = List.builder();
            while(channelResult.next()) {
                final UUID channelId = 
                    UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                channels.accept(
                    new Pair<String,Stored<Channel>>(
                        alias,channelStore.get(channelId)));
            }
            return (new Stored<Account>(new Account(user,channels.getList(),password),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up an account based on their username.
     */
    public Stored<Account> lookup(String username)
      throws DeletedException,
             SQLException {
        String sql = "SELECT Account.id from Account INNER JOIN User ON user=User.id where User.name=?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, username.toString());

        System.err.println(pre);
        
        final ResultSet rs = pre.executeQuery( );
        if(rs.next()) {
            final UUID identity = 
                    UUID.fromString(rs.getString("id"));
            return get(identity);
        }
        throw new DeletedException();
    }
    
} 
 
