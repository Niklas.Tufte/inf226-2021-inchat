package inf226.inchat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;
import inf226.util.*;



/**
 * The UserStore stores User objects in a SQL database.
 */
public final class UserStorage
    implements Storage<User,SQLException> {
    
    final Connection connection;
    
    public UserStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS User (id TEXT PRIMARY KEY, version TEXT, name TEXT, joined TEXT)");
    }
    
    @Override
    public Stored<User> save(User user)
      throws SQLException {
        final Stored<User> stored = new Stored<User>(user);
        String sql =  "INSERT INTO User VALUES(?,?,?,?)";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, stored.identity.toString());
        pre.setString(2, stored.version.toString());
        pre.setString(3, user.name.toString());
        pre.setString(4, user.joined.toString());

        pre.executeUpdate( );
        return stored;
    }
    
    @Override
    public synchronized Stored<User> update(Stored<User> user,
                                            User new_user)
        throws UpdatedException,
            DeletedException,
            SQLException {
        final Stored<User> current = get(user.identity);
        final Stored<User> updated = current.newVersion(new_user);
        if(current.version.equals(user.version)) {
        String sql = "UPDATE User SET" +" (version,name,joined) =(?,?,?) WHERE id=?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, updated.version.toString());
        pre.setString(2, new_user.name.toString());
        pre.setString(3, new_user.joined.toString());
        pre.setString(4, updated.identity.toString());

        pre.executeUpdate( );
        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<User> user)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<User> current = get(user.identity);
        if(current.version.equals(user.version)) {
            String sql =  "DELETE FROM User WHERE id =?";
            PreparedStatement pre = connection.prepareStatement( sql );
            pre.setString(1, user.identity.toString());
            pre.executeUpdate( );
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<User> get(UUID id)
      throws DeletedException,
             SQLException {
        String sql = "SELECT version,name,joined FROM User WHERE id = ?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, id.toString());
        final ResultSet rs = pre.executeQuery( );

        if(rs.next()) {
            final UUID version = 
                UUID.fromString(rs.getString("version"));
            final String name = rs.getString("name");
            final Instant joined = Instant.parse(rs.getString("joined"));
            return (new Stored<User>
                        (new User(name,joined),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up a user by their username;
     **/
    public Maybe<Stored<User>> lookup(String name) {
        String sql = "SELECT id FROM User WHERE name = ?";
        
        try{
            PreparedStatement pre = connection.prepareStatement( sql );
            pre.setString(1, name.toString());
            final ResultSet rs = pre.executeQuery( );
            if(rs.next())
                return Maybe.just(
                    get(UUID.fromString(rs.getString("id"))));
        } catch (Exception e) {
        
        }
        return Maybe.nothing();
    }
}


