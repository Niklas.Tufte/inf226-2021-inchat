package inf226.inchat;


public final class Encoder {
    public static String html(String input) {
        String output = input;
        output = output.replace("&","&amp");
        output = output.replace("<","&lt");
        output = output.replace(">","&gt");
        output = output.replace("\"","&#x22");
        output = output.replace("'", "&#x27");
        output = output.replace(" ", "&#x20");
        output = output.replace("%", "&#x25");
        output = output.replace("*", "&#x2A");
        output = output.replace("+", "&#x2B");
        output = output.replace(",", "&#x2C");
        output = output.replace("-", "&#x2D");
        output = output.replace("/", "&#x2F");
        output = output.replace(";", "&#x3B");
        output = output.replace("=", "&#x3D");
        output = output.replace("^", "&#x5E");
        output = output.replace("|", "&#x7C");



        return output;
    }
}