package inf226.inchat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);
        
        String sql =  "INSERT INTO Event VALUES(?,?,?,?,?)";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, stored.identity.toString());
        pre.setString(2, stored.version.toString());
        pre.setString(3, event.channel.toString());
        pre.setString(4, event.type.code.toString());
        pre.setString(5, event.time.toString());
        pre.executeUpdate( );

        switch (event.type) {
            case message:
                sql = "INSERT INTO Message VALUES(?,?,?)";
                pre = connection.prepareStatement( sql );
                pre.setString(1, stored.identity.toString());
                pre.setString(2, event.sender.toString());
                pre.setString(3, event.message.toString());
                break;
            case join:
                sql = "INSERT INTO Joined VALUES(?,?)";
                pre = connection.prepareStatement( sql );
                pre.setString(1, stored.identity.toString());
                pre.setString(2, event.sender.toString());
                break;
        }
        pre.executeUpdate( );
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        String sql = "UPDATE Event SET" +" (version,channel,time,type) =(?,?,?,?) WHERE id=?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, updated.version.toString());
        pre.setString(2, new_event.channel.toString());
        pre.setString(3, new_event.time.toString());
        pre.setString(4, new_event.type.code.toString());
        pre.setString(5, updated.identity.toString());
        pre.executeUpdate( );
        switch (new_event.type) {
            case message:
                sql = "UPDATE Message SET (sender,content)=(?,?) WHERE id=?";
                pre = connection.prepareStatement( sql );
                pre.setString(1, new_event.sender.toString());
                pre.setString(2, new_event.message.toString());
                pre.setString(3, updated.identity.toString());
                break;
            case join:
                sql = "UPDATE Joined SET (sender)=(?) WHERE id=?";
                pre = connection.prepareStatement( sql );
                pre.setString(1, new_event.sender.toString());
                pre.setString(2, updated.identity.toString());
                break;
        }
        pre.executeUpdate( );
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
        String sql =  "DELETE FROM Event WHERE id =?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, event.identity.toString());
        pre.executeUpdate( );
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        String sql = "SELECT version,channel,time,type FROM Event WHERE id = ?";
        PreparedStatement pre = connection.prepareStatement( sql );
        pre.setString(1, id.toString());
        final ResultSet rs = pre.executeQuery( );

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = 
                UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));
            
            switch(type) {
                case message:
                    String msql = "SELECT sender,content FROM Message WHERE id = ?";
                    PreparedStatement premsql = connection.prepareStatement( msql );
                    premsql.setString(1, id.toString());
                    final ResultSet mrs = premsql.executeQuery( );
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(channel,time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    String asql = "SELECT sender FROM Joined WHERE id = ?";
                    PreparedStatement preasql = connection.prepareStatement( asql );
                    preasql.setString(1, id.toString());
                    final ResultSet ars = preasql.executeQuery( );
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(channel,time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
